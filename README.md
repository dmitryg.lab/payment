#### Payment API

**Compact RESTful web service for payments processing.**

* GET /api/v1/secure/payments - fetching all payments
    * available params
        * amount - filter payments by amount
        * canceled(true|false) - filter payments cancel status
        * page(default 0) - number of page
        * size(default 20) - number elements on page
 * GET /api/v1/secure/payments/{id} - fetching payment by id
 * POST /api/v1/secure/payments - create payment
 * PUT /api/v1/secure/payments/cancel - cancel payment
 * POST /api/v1/user - create user 
 * POST /api/v1/auth - authentication
 
 ### Postman
 * ${path_to_project}/payment/postman
 
 ### Server
 * host - localhost
 * port - 8080
 
 ### Type of authentication
JSON Web Token
 
 #### How to build
 * cd ${path_to_project}/payment/devops
 * sudo bash build.sh

 
 ### How to stop
  * cd ${path_to_project}/payment/devops
  * sudo bash stop.sh