package com.computools.payment.service;

import com.computools.payment.dao.contract.FeeDataAccess;
import com.computools.payment.dao.contract.PaymentDataAccess;
import com.computools.payment.dao.contract.PaymentDetailsDataAccess;
import com.computools.payment.entity.Fee;
import com.computools.payment.entity.Payment;
import com.computools.payment.entity.PaymentDetails;
import com.computools.payment.entity.enums.Currency;
import com.computools.payment.entity.enums.PaymentType;
import com.computools.payment.exception.ApiException;
import com.computools.payment.util.PaymentUtil;
import com.computools.payment.validation.PaymentValidator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@ExtendWith(SpringExtension.class)
class PaymentServiceImplTest {

    @MockBean
    private AuthService authService;

    @MockBean
    private PaymentDataAccess paymentDataAccess;

    @MockBean
    private PaymentValidator mockPaymentValidator;

    @MockBean
    private FeeDataAccess feeDataAccess;

    @MockBean
    private PaymentDetailsDataAccess paymentDetailsDataAccess;

    private PaymentService paymentService;
    private PaymentValidator paymentValidator;

    private Payment payment;
    private PaymentDetails paymentDetails;
    private Fee fee;

    @BeforeEach
    public void init() {
        paymentService = new PaymentServiceImpl(authService, paymentDataAccess,
                paymentDetailsDataAccess, mockPaymentValidator, feeDataAccess);
        paymentValidator = new PaymentValidator();

        fee = new Fee();
        fee.setCurrency(Currency.EUR);

        paymentDetails = new PaymentDetails();
        paymentDetails.setDetails("test details");
        paymentDetails.setBicCode("bic code");

        payment = new Payment();
        payment.setPaymentType(PaymentType.TYPE1);
        payment.setCurrency(Currency.USD);
        payment.setPaymentDetails(paymentDetails);
    }

    @Test
    void create() {
        Mockito.when(paymentDetailsDataAccess.save(paymentDetails)).thenReturn(paymentDetails);
        paymentService.create(payment);
        Mockito.verify(authService, Mockito.times(1)).authenticate();
        Mockito.verify(mockPaymentValidator, Mockito.times(1)).validate(payment);
        Mockito.verify(paymentDataAccess, Mockito.times(1)).save(payment);

        Assertions.assertThrows(ApiException.class, () -> paymentValidator.validate(payment));

        payment.setCurrency(Currency.EUR);
        Assertions.assertDoesNotThrow(() -> paymentValidator.validate(payment));
        Assertions.assertNull(payment.getPaymentDetails().getBicCode());
    }

    @Test
    void cancel() {
        payment.setCanceled(true);
        payment.setId(1L) ;
        payment.setCurrency(Currency.EUR);
        payment.setCreationDate(LocalDateTime.now().minusHours(2));

        Mockito.when(paymentDataAccess.findById(1L)).thenReturn(payment);
        Assertions.assertThrows(ApiException.class, () -> paymentService.cancel(1L));

        payment.setCanceled(false);
        Mockito.when(paymentDataAccess.findById(1L)).thenReturn(payment);
        Assertions.assertDoesNotThrow(() -> paymentService.cancel(1L));

        payment.setCanceled(false);
        Mockito.when(paymentDataAccess.findById(1L)).thenReturn(payment);

        paymentService.cancel(1L);
        Mockito.verify(paymentDataAccess, Mockito.times(2)).save(payment);
        Assertions.assertNotNull(payment.getFee());
        Assertions.assertEquals(payment.getFee().getAmount(), new BigDecimal(PaymentUtil.TYPE1_COEFFICIENT).multiply(new BigDecimal(2)));
    }

}