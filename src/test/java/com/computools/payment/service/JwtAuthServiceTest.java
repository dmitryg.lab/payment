package com.computools.payment.service;

import com.computools.payment.dao.contract.UserDataAccess;
import com.computools.payment.dto.RequestAuthDTO;
import com.computools.payment.entity.User;
import com.computools.payment.exception.UserAuthenticationException;
import com.computools.payment.security.JwtResolver;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

@ExtendWith(SpringExtension.class)
class JwtAuthServiceTest {
    public static final String JWT = "eyJhbGciOiJIUzI1NiJ9.eyJpZCI6MywiZW1haWwiOiJqb2huLmRvdWVAY29tcHV0b29scy5jb20iLCJleHAiOjE2MTgxNDg4NDh9.zcXfUa76h_8E1pENyVQ0GRmXjX2yhtuV-N4yG0_RfMA";

    @MockBean
    private UserDataAccess userDataAccess;

    @MockBean
    private PasswordEncoder passwordEncoder;

    @MockBean
    private JwtResolver jwtResolver;

    private AuthService authService;
    private RequestAuthDTO requestAuthDTO;

    @BeforeEach
    public void init() {
        this.requestAuthDTO = new RequestAuthDTO();
        requestAuthDTO.setEmail("test@computools.com");
        requestAuthDTO.setPassword("qoesaAAsd");
        this.authService = new JwtAuthService(userDataAccess, passwordEncoder, jwtResolver);
    }

    @Test
    void authenticate() {
        var user = new User();
        user.setId(1L);
        user.setFirstName("testFirstName");
        user.setLastName("testLastName");
        Mockito.when(jwtResolver.createToken(user)).thenReturn(JWT);
        Mockito.when(userDataAccess.findByEmail(requestAuthDTO.getEmail())).thenReturn(Optional.of(user));
        Mockito.when(passwordEncoder.matches(requestAuthDTO.getPassword() ,user.getPassword())).thenReturn(true);
        authService.authenticate(requestAuthDTO);
        Mockito.verify(userDataAccess, Mockito.times(1)).findByEmail(requestAuthDTO.getEmail());
        Mockito.verify(jwtResolver, Mockito.times(1)).createToken(user);

        Mockito.when(userDataAccess.findByEmail(requestAuthDTO.getEmail())).thenReturn(Optional.empty());
        Assertions.assertThrows(UserAuthenticationException.class, () -> authService.authenticate(requestAuthDTO));

        Mockito.when(passwordEncoder.matches(requestAuthDTO.getPassword() ,user.getPassword())).thenReturn(true);
        Assertions.assertThrows(UserAuthenticationException.class, () -> authService.authenticate(requestAuthDTO));
    }
}