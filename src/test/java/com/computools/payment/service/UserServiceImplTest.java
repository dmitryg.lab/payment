package com.computools.payment.service;

import com.computools.payment.dao.UserDataAccessImpl;
import com.computools.payment.dao.contract.UserDataAccess;
import com.computools.payment.entity.User;
import com.computools.payment.exception.ApiException;
import com.computools.payment.repository.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

@ExtendWith(SpringExtension.class)
class UserServiceImplTest {

    public static final String HASHED_PASSWORD = "$2a$10$YCch79Bgd58g5/ToxfGVzO6IqmL/TGC.x2mKB/nvCYlaHPKc9UHFe";
    public static final String PASSWORD = "qoesaAAsd";
    public static final Long ID = 1L;

    @MockBean
    private  UserDataAccess userDataAccess;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private  PasswordEncoder passwordEncoder;

    private UserService mockUserService;

    private User user;

    @BeforeEach
    public void init() {
        this.user = new User();
        this.user.setEmail("test@computools.com");
        this.user.setFirstName("TestFirstName");
        this.user.setLastName("TestLastName");
        this.user.setPassword(PASSWORD);;
        this.mockUserService = new UserServiceImpl(userDataAccess, passwordEncoder);
    }

    @Test
    void create() {
        Mockito.when(passwordEncoder.encode(PASSWORD)).thenReturn(HASHED_PASSWORD);
        mockUserService.create(user);
        Mockito.verify(passwordEncoder, Mockito.times(1)).encode(PASSWORD);
        Mockito.verify(userDataAccess, Mockito.times(1)).findByEmail(user.getEmail());
        Mockito.verify(userDataAccess, Mockito.times(1)).save(user);
        Assertions.assertEquals(HASHED_PASSWORD, user.getPassword());
        Mockito.when(userDataAccess.findByEmail(user.getEmail())).thenReturn(Optional.ofNullable(new User()));
        Assertions.assertThrows(ApiException.class, () -> mockUserService.create(user));
    }

    @Test
    void findById() {
        var user = new User();
        user.setId(ID);
        Mockito.when(userDataAccess.findById(ID)).thenReturn(user);
        mockUserService.findById(ID);
        Mockito.verify(userDataAccess, Mockito.times(1)).findById(ID);
        Assertions.assertEquals(mockUserService.findById(ID).getId(), user.getId());
        var mockUserDataAccess = new UserDataAccessImpl(userRepository);
        Mockito.when(userRepository.findById(ID)).thenReturn(Optional.empty());
        Assertions.assertThrows(ApiException.class, () -> mockUserDataAccess.findById(ID));
    }
}