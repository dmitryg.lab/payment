CREATE TABLE IF NOT EXISTS users (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    first_name      VARCHAR(100),
    last_name VARCHAR (100),
    email VARCHAR (100) NOT NULL UNIQUE ,
    password VARCHAR
);

CREATE TABLE  IF NOT EXISTS payment_details(
    id BIGSERIAL NOT NULL PRIMARY KEY,
    debtor_iban VARCHAR NOT NULL,
    creditor_iban VARCHAR NOT NULL,
    details TEXT,
    bic_code TEXT
);

CREATE TABLE IF NOT EXISTS fee (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    amount NUMERIC(3, 2),
    currency VARCHAR(3) NOT NULL
);

CREATE TABLE IF NOT EXISTS payments (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    amount NUMERIC(12, 2) NOT NULL,
    currency VARCHAR(3) NOT NULL,
    payment_type VARCHAR(5) NOT NULL,
    creation_date TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    is_canceled BOOLEAN NOT NULL,
    user_id BIGINT,
    payment_details_id BIGINT,
    fee_id BIGINT,
    CONSTRAINT fee_fk FOREIGN KEY (fee_id) REFERENCES fee(id),
    CONSTRAINT payment_details_fk FOREIGN KEY (payment_details_id) REFERENCES payment_details(id),
    CONSTRAINT user_fk FOREIGN KEY (user_id) REFERENCES users(id)
);