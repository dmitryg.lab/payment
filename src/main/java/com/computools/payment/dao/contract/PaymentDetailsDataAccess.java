package com.computools.payment.dao.contract;

import com.computools.payment.entity.PaymentDetails;

public interface PaymentDetailsDataAccess {
    PaymentDetails save(PaymentDetails paymentDetails);
}
