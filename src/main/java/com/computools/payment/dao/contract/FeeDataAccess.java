package com.computools.payment.dao.contract;

import com.computools.payment.entity.Fee;

public interface FeeDataAccess {
    Fee save(Fee fee);
}
