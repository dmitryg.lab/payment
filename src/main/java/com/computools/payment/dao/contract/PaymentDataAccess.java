package com.computools.payment.dao.contract;

import com.computools.payment.dto.PaymentParams;
import com.computools.payment.entity.Payment;
import com.computools.payment.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface PaymentDataAccess {
    Payment save(Payment payment);
    Payment findById(Long id);
    Payment findByUserAndId(User user, Long id);
    Page<Payment> findByParams(PaymentParams paymentParams, Pageable pageable, User user);
}
