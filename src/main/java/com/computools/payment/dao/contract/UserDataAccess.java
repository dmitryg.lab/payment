package com.computools.payment.dao.contract;

import com.computools.payment.entity.User;

import java.util.Optional;

public interface UserDataAccess {
    User save(User user);
    User findById(Long id);
    Optional<User> findByEmail(String email);
}
