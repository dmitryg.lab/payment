package com.computools.payment.dao;

import com.computools.payment.dao.contract.FeeDataAccess;
import com.computools.payment.entity.Fee;
import com.computools.payment.repository.FeeRepository;
import org.springframework.stereotype.Repository;

@Repository
public class FeeDataAccessImpl implements FeeDataAccess {
    private final FeeRepository feeRepository;

    public FeeDataAccessImpl(FeeRepository feeRepository) {
        this.feeRepository = feeRepository;
    }

    @Override
    public Fee save(Fee fee) {
        return feeRepository.save(fee);
    }
}
