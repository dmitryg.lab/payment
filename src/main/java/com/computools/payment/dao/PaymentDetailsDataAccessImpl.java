package com.computools.payment.dao;

import com.computools.payment.dao.contract.PaymentDetailsDataAccess;
import com.computools.payment.entity.PaymentDetails;
import com.computools.payment.repository.PaymentDetailsRepository;
import org.springframework.stereotype.Repository;

@Repository
public class PaymentDetailsDataAccessImpl implements PaymentDetailsDataAccess {
    private final PaymentDetailsRepository paymentDetailsRepository;

    public PaymentDetailsDataAccessImpl(PaymentDetailsRepository paymentDetailsRepository) {
        this.paymentDetailsRepository = paymentDetailsRepository;
    }

    @Override
    public PaymentDetails save(PaymentDetails paymentDetails) {
        return paymentDetailsRepository.save(paymentDetails);
    }
}
