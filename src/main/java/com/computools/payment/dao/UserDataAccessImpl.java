package com.computools.payment.dao;

import com.computools.payment.dao.contract.UserDataAccess;
import com.computools.payment.entity.User;
import com.computools.payment.exception.ApiException;
import com.computools.payment.repository.UserRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class UserDataAccessImpl implements UserDataAccess {

    private final UserRepository userRepository;

    public UserDataAccessImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User save(User user) {
        return userRepository.save(user);
    }

    @Override
    public User findById(Long id) {
        return userRepository.findById(id)
                .orElseThrow(() ->
                        new ApiException(String.format("User with id %s doesn't exists", id), HttpStatus.NOT_FOUND.value()));
    }

    @Override
    public Optional<User> findByEmail(String email) {
        return userRepository.findByEmail(email);
    }
}
