package com.computools.payment.dao;

import com.computools.payment.dao.contract.PaymentDataAccess;
import com.computools.payment.dto.PaymentParams;
import com.computools.payment.entity.Payment;
import com.computools.payment.entity.User;
import com.computools.payment.exception.ApiException;
import com.computools.payment.repository.PaymentRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

@Repository
public class PaymentDataAccessImpl implements PaymentDataAccess {
    private final PaymentRepository paymentRepository;

    public PaymentDataAccessImpl(PaymentRepository paymentRepository) {
        this.paymentRepository = paymentRepository;
    }

    @Override
    public Payment save(Payment payment) {
        return paymentRepository.save(payment);
    }

    @Override
    public Payment findById(Long id) {
        return paymentRepository.findById(id)
                .orElseThrow(() -> new ApiException(String.format("Payment with id %s doesn't exists", id),
                        HttpStatus.NOT_FOUND.value()));
    }

    @Override
    public Payment findByUserAndId(User user, Long id) {
        return paymentRepository.findByUserAndId(user, id)
                .orElseThrow(() -> new ApiException(String.format("Payment with id %s doesn't exists", id),
                        HttpStatus.NOT_FOUND.value()));
    }

    @Override
    public Page<Payment> findByParams(PaymentParams paymentParams, Pageable pageable, User user) {
        return paymentRepository.findAll((root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            Join<Payment, User> userJoin = root.join("user");
            predicates.add(criteriaBuilder.equal(userJoin.get("id"), user.getId()));
            if (paymentParams.getCanceled() != null) {
                predicates.add(criteriaBuilder.equal(root.get("isCanceled"), paymentParams.getCanceled()));
            }
            if (paymentParams.getAmount() != null) {
                predicates.add(criteriaBuilder.equal(root.get("amount"), paymentParams.getAmount()));
            }
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        }, pageable);
    }
}
