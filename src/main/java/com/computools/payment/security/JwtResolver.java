package com.computools.payment.security;

import com.computools.payment.entity.User;
import com.computools.payment.exception.UserAuthenticationException;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Date;

@Component
public class JwtResolver {
    private final String secret;
    private final long expiration;

    public JwtResolver(@Value("${security.secret}") String secret,
                       @Value("${security.expiration}") long expiration) {
        this.secret = secret;
        this.expiration = expiration;
    }

    public String createToken(User user) {
        return Jwts.builder()
                .claim("id", user.getId())
                .claim("email", user.getEmail())
                .signWith(SignatureAlgorithm.HS256, secret.getBytes())
                .setExpiration(new Date(System.currentTimeMillis() + expiration))
                .compact();
    }

    public JwtUserDetails validate(String token) {
        if (!StringUtils.hasText(token)) {
            throw new UserAuthenticationException("Authorization error", HttpStatus.UNAUTHORIZED.value());
        }
        try {
            token = token.substring(7);
            Jws<Claims> claimsJws = Jwts.parser().setSigningKey(secret.getBytes()).parseClaimsJws(token);
            return new JwtUserDetails((String) claimsJws.getBody().get("email"));
        } catch (Exception e) {
            throw new UserAuthenticationException("Authorization error", HttpStatus.UNAUTHORIZED.value());
        }
    }

}
