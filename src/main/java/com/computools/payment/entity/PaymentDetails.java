package com.computools.payment.entity;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Entity
public class PaymentDetails extends BaseEntity {
    private String debtorIban;
    private String creditorIban;
    private String details;
    private String bicCode;

    @OneToOne(mappedBy = "paymentDetails")
    private Payment payment;

    public String getDebtorIban() {
        return debtorIban;
    }

    public void setDebtorIban(String debtorIban) {
        this.debtorIban = debtorIban;
    }

    public String getCreditorIban() {
        return creditorIban;
    }

    public void setCreditorIban(String creditorIban) {
        this.creditorIban = creditorIban;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getBicCode() {
        return bicCode;
    }

    public void setBicCode(String bicCode) {
        this.bicCode = bicCode;
    }

    public Payment getPayment() {
        return payment;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }
}
