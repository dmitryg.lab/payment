package com.computools.payment.entity;

import com.computools.payment.entity.enums.Currency;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToOne;
import java.math.BigDecimal;

@Entity
public class Fee extends BaseEntity {
    private BigDecimal amount;

    @Enumerated(EnumType.STRING)
    private Currency currency = Currency.EUR;

    @OneToOne(mappedBy = "fee")
    private Payment payment;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Payment getPayment() {
        return payment;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }
}
