package com.computools.payment.entity.enums;

public enum Currency {
    USD, EUR
}
