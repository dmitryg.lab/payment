package com.computools.payment.validation;

import com.computools.payment.entity.Payment;
import com.computools.payment.entity.enums.Currency;
import com.computools.payment.exception.ApiException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class PaymentValidator  {

    public void validate(Payment payment) {
        var paymentDetails = payment.getPaymentDetails();
        switch (payment.getPaymentType()) {
            case TYPE1:
                if (!payment.getCurrency().equals(Currency.EUR) || !StringUtils.hasText(paymentDetails.getDetails())) {
                    throw new ApiException("Type1 payment can be applied only in EUR currency and require details",
                            HttpStatus.BAD_REQUEST.value());
                }
                paymentDetails.setBicCode(null);
                break;
            case TYPE2:
                if (!payment.getCurrency().equals(Currency.USD)) {
                    throw new ApiException("Type2 payment can be applied only in USD currency", HttpStatus.BAD_REQUEST.value());
                }
                paymentDetails.setBicCode(null);
                break;
            case TYPE3:
                if (!StringUtils.hasText(paymentDetails.getBicCode())) {
                    throw new ApiException("Type3 payment require bicCode",
                            HttpStatus.BAD_REQUEST.value());
                }
                paymentDetails.setDetails(null);
        }
    }

}
