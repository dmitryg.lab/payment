package com.computools.payment.controller;

import com.computools.payment.dto.PaymentDTO;
import com.computools.payment.dto.PaymentParams;
import com.computools.payment.mapper.PaymentMapper;
import com.computools.payment.service.PaymentService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


@RestController
@Transactional
@RequestMapping("/secure/payments")
public class PaymentController {
    private final PaymentService paymentService;
    private final PaymentMapper paymentMapper;

    public PaymentController(PaymentService paymentService, PaymentMapper paymentMapper) {
        this.paymentService = paymentService;
        this.paymentMapper = paymentMapper;
    }

    @GetMapping("/{id}")
    public PaymentDTO findById(@PathVariable Long id) {
        return paymentMapper.map(paymentService.findById(id));
    }

    @GetMapping
    public Page<PaymentDTO> findAll(PaymentParams paymentParams, Pageable pageable) {
        return paymentMapper.map(paymentService.findAll(paymentParams, pageable), pageable);
    }

    @PostMapping
    public PaymentDTO create(@Validated @RequestBody PaymentDTO paymentDTO) {
        return paymentMapper.map(paymentService.create(paymentMapper.map(paymentDTO)));
    }

    @PutMapping("/cancel/{id}")
    public PaymentDTO cancel(@PathVariable Long id) {
        return paymentMapper.map(paymentService.cancel(id));
    }

}
