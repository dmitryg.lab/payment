package com.computools.payment.controller;

import com.computools.payment.dto.RequestAuthDTO;
import com.computools.payment.dto.ResponseAuthDTO;
import com.computools.payment.service.AuthService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/auth")
public class AuthController {

    private final AuthService authService;

    public AuthController(AuthService authService) {
        this.authService = authService;
    }

    @PostMapping
    public ResponseAuthDTO login(@RequestBody RequestAuthDTO authDTO) {
        return authService.authenticate(authDTO);
    }

}
