package com.computools.payment.controller;

import com.computools.payment.dto.UserDTO;
import com.computools.payment.mapper.UserMapper;
import com.computools.payment.service.UserService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Transactional
@RequestMapping("/users")
public class UserController {
    private final UserService userService;
    private final UserMapper userMapper;

    public UserController(UserService userService, UserMapper userMapper) {
        this.userService = userService;
        this.userMapper = userMapper;
    }

    @PostMapping
    public UserDTO create(@Validated @RequestBody UserDTO userDTO) {
        return userMapper.map(userService.create(userMapper.map(userDTO)));
    }
}
