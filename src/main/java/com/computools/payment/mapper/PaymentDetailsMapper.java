package com.computools.payment.mapper;

import com.computools.payment.dto.PaymentDetailsDTO;
import com.computools.payment.entity.PaymentDetails;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface PaymentDetailsMapper {
    PaymentDetails map(PaymentDetailsDTO paymentDetailsDto);
    PaymentDetailsDTO map(PaymentDetails paymentDetails);
}
