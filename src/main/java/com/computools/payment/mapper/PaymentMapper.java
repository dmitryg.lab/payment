package com.computools.payment.mapper;

import com.computools.payment.dto.PaymentDTO;
import com.computools.payment.entity.Payment;
import org.mapstruct.Mapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.stream.Collectors;

@Mapper(componentModel = "spring", uses = {PaymentDetailsMapper.class})
public interface PaymentMapper {
    PaymentDTO map(Payment payment);
    Payment map(PaymentDTO paymentDTO);
    default Page<PaymentDTO> map(Page<Payment> payments, Pageable pageable) {
        return new PageImpl<>(payments.getContent().stream().map(this::map).collect(Collectors.toList()),
                pageable, payments.getTotalElements());
    }
}
