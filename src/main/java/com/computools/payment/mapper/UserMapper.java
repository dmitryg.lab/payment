package com.computools.payment.mapper;

import com.computools.payment.dto.UserDTO;
import com.computools.payment.entity.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface UserMapper {
    User map(UserDTO userDTO);

    @Mapping(target = "password", ignore = true)
    UserDTO map(User user);
}
