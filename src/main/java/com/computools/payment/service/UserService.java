package com.computools.payment.service;

import com.computools.payment.entity.User;

public interface UserService {
    User create(User user);
    User findById(Long id);
}
