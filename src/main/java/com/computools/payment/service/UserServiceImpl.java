package com.computools.payment.service;

import com.computools.payment.dao.contract.UserDataAccess;
import com.computools.payment.entity.User;
import com.computools.payment.exception.ApiException;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
    private final UserDataAccess userDataAccess;
    private final PasswordEncoder passwordEncoder;

    public UserServiceImpl(UserDataAccess userDataAccess, PasswordEncoder passwordEncoder) {
        this.userDataAccess = userDataAccess;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public User create(User user) {
        if (userDataAccess.findByEmail(user.getEmail()).isPresent()) {
            throw new ApiException(String.format("User with email %s already exist", user.getEmail()), HttpStatus.CONFLICT.value());
        }
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return userDataAccess.save(user);
    }

    @Override
    public User findById(Long id) {
        return userDataAccess.findById(id);
    }
}
