package com.computools.payment.service;

import com.computools.payment.dao.contract.UserDataAccess;
import com.computools.payment.dto.RequestAuthDTO;
import com.computools.payment.dto.ResponseAuthDTO;
import com.computools.payment.entity.User;
import com.computools.payment.exception.UserAuthenticationException;
import com.computools.payment.security.JwtResolver;
import com.computools.payment.security.JwtUserDetails;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class JwtAuthService implements AuthService {
    private final UserDataAccess userDataAccess;
    private final PasswordEncoder passwordEncoder;
    private final JwtResolver jwtResolver;

    public JwtAuthService(UserDataAccess userDataAccess, PasswordEncoder passwordEncoder, JwtResolver jwtResolver) {
        this.userDataAccess = userDataAccess;
        this.passwordEncoder = passwordEncoder;
        this.jwtResolver = jwtResolver;
    }

    @Override
    public ResponseAuthDTO authenticate(RequestAuthDTO authDTO) {
        var user = userDataAccess.findByEmail(authDTO.getEmail());
        if (user.isEmpty() || !passwordEncoder.matches(authDTO.getPassword(), user.get().getPassword())) {
            throw new UserAuthenticationException("Authentication error", HttpStatus.UNAUTHORIZED.value());
        }
        return new ResponseAuthDTO(jwtResolver.createToken(user.get()), user.get().getFirstName(),
                user.get().getLastName(), user.get().getId());
    }

    @Override
    public User authenticate() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null || authentication.getPrincipal() == null) {
            throw new UserAuthenticationException("Authentication error", HttpStatus.UNAUTHORIZED.value());
        }
        return userDataAccess.findByEmail(((JwtUserDetails)authentication.getPrincipal()).getUsername())
                .orElseThrow(() ->  new UserAuthenticationException("Authentication error", HttpStatus.UNAUTHORIZED.value()));
    }
}
