package com.computools.payment.service;

import com.computools.payment.dto.PaymentParams;
import com.computools.payment.entity.Payment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface PaymentService {
    Payment create(Payment payment);
    Payment cancel(Long id);
    Payment findById(Long id);
    Page<Payment> findAll(PaymentParams params, Pageable pageable);
}
