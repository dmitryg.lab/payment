package com.computools.payment.service;

import com.computools.payment.dto.RequestAuthDTO;
import com.computools.payment.dto.ResponseAuthDTO;
import com.computools.payment.entity.User;

public interface AuthService {
    ResponseAuthDTO authenticate(RequestAuthDTO authDTO);
    User authenticate();
}
