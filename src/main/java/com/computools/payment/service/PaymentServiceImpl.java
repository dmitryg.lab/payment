package com.computools.payment.service;

import com.computools.payment.dao.contract.FeeDataAccess;
import com.computools.payment.dao.contract.PaymentDataAccess;
import com.computools.payment.dao.contract.PaymentDetailsDataAccess;
import com.computools.payment.dto.PaymentParams;
import com.computools.payment.entity.Fee;
import com.computools.payment.entity.Payment;
import com.computools.payment.entity.enums.PaymentType;
import com.computools.payment.exception.ApiException;
import com.computools.payment.util.PaymentUtil;
import com.computools.payment.validation.PaymentValidator;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

@Service
public class PaymentServiceImpl implements PaymentService {
    private final AuthService authService;
    private final PaymentDataAccess paymentDataAccess;
    private final PaymentDetailsDataAccess paymentDetailsDataAccess;
    private final FeeDataAccess feeDataAccess;
    private final PaymentValidator paymentValidator;

    public PaymentServiceImpl(AuthService authService, PaymentDataAccess paymentDataAccess,
                              PaymentDetailsDataAccess paymentDetailsDataAccess,
                              PaymentValidator paymentValidator, FeeDataAccess feeDataAccess) {
        this.authService = authService;
        this.paymentDataAccess = paymentDataAccess;
        this.paymentDetailsDataAccess = paymentDetailsDataAccess;
        this.paymentValidator = paymentValidator;
        this.feeDataAccess = feeDataAccess;
    }

    @Override
    public Payment create(Payment payment) {
        payment.setUser(authService.authenticate());
        paymentValidator.validate(payment);
        payment.setPaymentDetails(paymentDetailsDataAccess.save(payment.getPaymentDetails()));
        return paymentDataAccess.save(payment);
    }

    @Override
    public Payment cancel(Long id) {
        Payment payment = paymentDataAccess.findById(id);
        if (!LocalDate.now().isEqual(payment.getCreationDate().toLocalDate())) {
            throw new ApiException("It is possible to cancel payment only on the day of creation",
                    HttpStatus.BAD_REQUEST.value());
        }
        if (payment.isCanceled()) {
            throw new ApiException(String.format("Payment with id %s already canceled", id),
                    HttpStatus.BAD_REQUEST.value());
        }
        /*
        * calculate fee as: h * k
        * where h - number of full hours (2:59 = 2h) payment is in system;
        * k - coefficient (0.05 for TYPE1; 0.1 for TYPE2, 0.15 for TYPE3).
        */
        var fee = calculateFee(payment.getPaymentType(), (int) Math.floor(
                Math.abs(LocalDateTime.now().until(payment.getCreationDate(), ChronoUnit.MINUTES)) / 60.0));
        payment.setFee(fee);
        feeDataAccess.save(fee);
        payment.setCanceled(true);
        paymentDataAccess.save(payment);
        return payment;
    }

    @Override
    public Payment findById(Long id) {
        return paymentDataAccess.findByUserAndId(authService.authenticate(), id);
    }

    @Override
    public Page<Payment> findAll(PaymentParams params, Pageable pageable) {
        return paymentDataAccess.findByParams(params, pageable, authService.authenticate());
    }

    private Fee calculateFee(PaymentType paymentType, int hours) {
        var fee = new Fee();
        BigDecimal amount;
        switch (paymentType) {
            case TYPE1:
                amount = new BigDecimal(PaymentUtil.TYPE1_COEFFICIENT);
                break;
            case TYPE2:
                amount = new BigDecimal(PaymentUtil.TYPE2_COEFFICIENT);
                break;
            default:
                amount = new BigDecimal(PaymentUtil.TYPE3_COEFFICIENT);
        }
        fee.setAmount(amount.multiply(new BigDecimal(hours)));
        return fee;
    }

}
