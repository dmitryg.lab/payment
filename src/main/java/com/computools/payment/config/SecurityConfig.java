package com.computools.payment.config;

import com.computools.payment.exception.dto.ExceptionResponseDTO;
import com.computools.payment.security.JwtFilter;
import com.computools.payment.security.JwtProvider;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.Filter;
import java.time.LocalDateTime;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    public static final String SECURE_PATH = "/secure/**";
    private final JwtProvider jwtProvider;
    private final ObjectMapper objectMapper;

    public SecurityConfig(JwtProvider jwtProvider, ObjectMapper objectMapper) {
        this.jwtProvider = jwtProvider;
        this.objectMapper = objectMapper;
    }


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .cors().disable()
                .csrf().disable()
                .httpBasic().disable()
                .authorizeRequests()
                .antMatchers("/payments")
                .authenticated()
                .and()
                .addFilterBefore(filter(), UsernamePasswordAuthenticationFilter.class);
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    public Filter filter() {
        JwtFilter jwtFilter = new JwtFilter(SECURE_PATH, jwtProvider);
        jwtFilter.setAuthenticationSuccessHandler((httpServletRequest, httpServletResponse, authentication) -> {});
        jwtFilter.setAuthenticationFailureHandler((httpServletRequest, httpServletResponse, e) -> {
            httpServletResponse.setStatus(HttpStatus.UNAUTHORIZED.value());
            httpServletResponse.setHeader("Content-type", "application/json");
            httpServletResponse.getOutputStream().println(
                    objectMapper.writeValueAsString(
                            new ExceptionResponseDTO("Authentication error", HttpStatus.UNAUTHORIZED.value(), LocalDateTime.now())));
        });
        return jwtFilter;
    }
}
