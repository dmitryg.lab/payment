package com.computools.payment.location;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;

@Aspect
@Component
public class LocationLogger {

    @Autowired(required = false)
    private HttpServletRequest request;

    private final Logger logger = LoggerFactory.getLogger(LocationLogger.class);


    @Before("within(@org.springframework.web.bind.annotation.RestController* *) && execution(* *(..))")
    public void log() {
        RestTemplate restTemplate = new RestTemplate();
        try {
            String countryName = restTemplate.getForObject(String.format("https://ipapi.co/%s/country_name/", request.getRemoteAddr()), String.class);
            if (request != null) {
                logger.info(String.format("%s %s request from %s", request.getMethod(),
                        request.getRequestURI(), countryName));
            }
        } catch(RestClientException e) {
            logger.error("Failed to get country info");
            e.printStackTrace();
        }
    }

}
