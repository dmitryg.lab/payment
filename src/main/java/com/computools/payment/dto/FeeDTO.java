package com.computools.payment.dto;

import com.computools.payment.entity.enums.Currency;

import java.math.BigDecimal;

public class FeeDTO extends BaseDTO {
    private BigDecimal amount;
    private Currency currency;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }
}
