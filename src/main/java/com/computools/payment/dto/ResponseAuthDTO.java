package com.computools.payment.dto;

public class ResponseAuthDTO extends BaseDTO {
    private String token;
    private String firstName;
    private String lastName;

    public ResponseAuthDTO(String token, String firstName, String lastName, Long id) {
        super(id);
        this.token = token;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getToken() {
        return token;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
}
