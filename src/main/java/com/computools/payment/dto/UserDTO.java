package com.computools.payment.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class UserDTO extends BaseDTO {

    @NotNull(message = "First name should be specified")
    @NotBlank(message = "first name should be specified")
    private String firstName;

    @NotNull(message = "Last name should be specified")
    @NotBlank(message = "Last name should be specified")
    private String lastName;

    @Email(message = "Wrong email format")
    @NotNull(message = "Email should be specified")
    @NotBlank(message = "Email should be specified")
    private String email;

    @NotNull(message = "Password should be specified")
    @NotBlank(message = "Password should be specified")
    @Length(min = 8, message = "Length of password should be at least 8 characters")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String password;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
