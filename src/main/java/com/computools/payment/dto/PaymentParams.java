package com.computools.payment.dto;

import java.math.BigDecimal;

public class PaymentParams {
    private Boolean isCanceled;
    private BigDecimal amount;

    public Boolean getCanceled() {
        return isCanceled;
    }

    public void setCanceled(Boolean canceled) {
        isCanceled = canceled;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
