package com.computools.payment.dto;

import com.computools.payment.entity.enums.Currency;
import com.computools.payment.entity.enums.PaymentType;
import com.fasterxml.jackson.annotation.JsonInclude;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;

public class PaymentDTO extends BaseDTO {

    @NotNull(message = "Amount should be specified")
    @Positive
    private BigDecimal amount;

    @NotNull(message = "Currency should be specified")
    private Currency currency;

    @NotNull(message = "Payment type should be specified")
    private PaymentType paymentType;

    private boolean isCanceled;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private FeeDTO fee;

    @NotNull(message = "Payment details should be specified")
    @Valid
    private PaymentDetailsDTO paymentDetails;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public PaymentType getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(PaymentType paymentType) {
        this.paymentType = paymentType;
    }

    public boolean isCanceled() {
        return isCanceled;
    }

    public void setCanceled(boolean canceled) {
        isCanceled = canceled;
    }

    public FeeDTO getFee() {
        return fee;
    }

    public void setFee(FeeDTO fee) {
        this.fee = fee;
    }

    public PaymentDetailsDTO getPaymentDetails() {
        return paymentDetails;
    }

    public void setPaymentDetails(PaymentDetailsDTO paymentDetails) {
        this.paymentDetails = paymentDetails;
    }
}
