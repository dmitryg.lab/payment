package com.computools.payment.exception.dto;

import java.time.LocalDateTime;

public class ExceptionResponseDTO {
    private String message;
    private int code;
    private LocalDateTime timestamp;

    public ExceptionResponseDTO(String message, int code, LocalDateTime timestamp) {
        this.message = message;
        this.code = code;
        this.timestamp = timestamp;
    }

    public String getMessage() {
        return message;
    }

    public int getCode() {
        return code;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }
}
