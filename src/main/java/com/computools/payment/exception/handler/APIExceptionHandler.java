package com.computools.payment.exception.handler;

import com.computools.payment.exception.ApiException;
import com.computools.payment.exception.UserAuthenticationException;
import com.computools.payment.exception.dto.ExceptionResponseDTO;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.LocalDateTime;
import java.util.Objects;

@RestControllerAdvice
public class APIExceptionHandler {

    @ExceptionHandler(ApiException.class)
    public ExceptionResponseDTO handle(ApiException e) {
        return new ExceptionResponseDTO(e.getMessage(), e.code, e.timestamp);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ExceptionResponseDTO handle(MethodArgumentNotValidException e) {
        return new ExceptionResponseDTO(e.getAllErrors().stream()
                .filter(Objects::nonNull)
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .findFirst().orElse("Bad request"), HttpStatus.BAD_REQUEST.value(), LocalDateTime.now());
    }

    @ExceptionHandler(UserAuthenticationException.class)
    public ExceptionResponseDTO handle(UserAuthenticationException e) {
        return new ExceptionResponseDTO(e.getMessage(), e.code, e.timestamp);
    }

}
