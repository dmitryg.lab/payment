package com.computools.payment.exception;

import org.springframework.security.core.AuthenticationException;

import java.time.LocalDateTime;

public class UserAuthenticationException extends AuthenticationException {
    public int code;
    public LocalDateTime timestamp = LocalDateTime.now();
    public UserAuthenticationException(String message, int code) {
        super(message);
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
