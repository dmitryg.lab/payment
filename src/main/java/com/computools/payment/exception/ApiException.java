package com.computools.payment.exception;

import java.time.LocalDateTime;

public class ApiException extends RuntimeException {
    public int code;
    public LocalDateTime timestamp = LocalDateTime.now();

    public ApiException(String message, int code) {
        super(message);
        this.code = code;
    }
}
