cd ../

# install docker engine if not exists
if ! command -v docker; then
    echo "Docker engine not found"
    echo "Installing docker engine"
    sudo apt-get update
    sudo apt-get install docker-ce docker-ce-cli containerd.io -y
else
    echo "Docker engine is detected"
fi

# install docker compose if not exists
if ! command -v docker-compose; then
    echo "Docker compose not found"
    echo "Installing docker-compose"
    sudo curl -L "https://github.com/docker/compose/releases/download/1.29.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
    sudo chmod +x /usr/local/bin/docker-composeenable
else
    echo "Docker compose is detected"
fi

# install maven if not exists
if ! command -v mvn; then
  sudo apt-get install maven
else
  echo "Maven is detected"
fi

# build application
mvn clean package
docker-compose -f devops/docker-compose.yml up --build -d