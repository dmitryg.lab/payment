FROM ubuntu:18.04
FROM openjdk:11
WORKDIR /usr/app

COPY target/payment-0.0.1-SNAPSHOT.jar .

ENTRYPOINT exec java -jar payment-0.0.1-SNAPSHOT.jar